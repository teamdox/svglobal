defmodule Svglobal.Mixfile do
  use Mix.Project

  def project do
    [
      app: :svglobal,
      version: "0.1.0",
      elixir: "~> 1.7",
      deps: deps(),
      name: "Svglobal",
      description: "Svg para los proyectos con Phoenix Liveview",
      package: package(),
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps, do: []

  defp package do
    [
      maintainers: ["Leonardo E. Reyna Castro"],
      licenses: ["MIT"],
      links: %{bitbucket: "https://bitbucket.org/teamdox/svglobal"},
      files: ~w(priv mix.exs README.md)
    ]
  end
end
